package isp.lab9.exercise2;

import java.util.Arrays;
import java.util.List;

public class Client {
    public static void main(String[] args) {

        final SensorController sensorController = new SensorController();


        final List<Observable> sensorOperations = Arrays.asList(
                new TemperatureSensor(),
                new HumiditySensor(),
                new PressureSensor()
        );

        // register observer for each observable
        sensorOperations.forEach(sensor -> sensor.register(sensorController));

        // change some data
        sensorOperations.forEach(sensor -> ((SensorOperations)sensor).readSensor());
        sensorOperations.forEach(sensor -> ((SensorOperations)sensor).readSensor());
    }
}