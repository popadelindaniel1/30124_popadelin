import isp.lab5.exercise1.ATM;
import isp.lab5.exercise1.Bank;
import isp.lab5.exercise1.Card;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Test_Ex1 {

    @Test
    public void test_InsertCard(){
        Bank bank = new Bank();
        ATM atm = new ATM(bank);
        Card card = new Card("23443421", "2345X");

        atm.insertCard(card, "2345X");


        assertTrue("Result is true ", atm.insertCard(card, "2345X") == true);

    }
}

