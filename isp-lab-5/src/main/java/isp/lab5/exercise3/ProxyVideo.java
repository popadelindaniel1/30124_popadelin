package isp.lab5.exercise3;

class ProxyVideo implements Playable {

    private ColorVideo video;
    private String fileName;
    private BlackAndWhiteVideo blackWhiteVideo;

    public ProxyVideo(String fileName){
        this.fileName = fileName;
    }

    public ProxyVideo(String fileName, ColorVideo video){
        this.fileName = fileName;
        this.video = new ColorVideo(fileName);
    }

    public ProxyVideo(String fileName, BlackAndWhiteVideo blackWhiteVideo){
        this.fileName = fileName;
        this.blackWhiteVideo = new BlackAndWhiteVideo(fileName);
    }

    @Override
    public void play() {
        if(video == null){
            video = new ColorVideo(fileName);
        }
        video.play();

        if(blackWhiteVideo == null){
            blackWhiteVideo = new BlackAndWhiteVideo(fileName);
        }
        blackWhiteVideo.play();
    }
}