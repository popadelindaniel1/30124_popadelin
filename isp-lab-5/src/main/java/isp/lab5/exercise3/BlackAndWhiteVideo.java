package isp.lab5.exercise3;

public class BlackAndWhiteVideo implements Playable {

    private String fileName;

    public BlackAndWhiteVideo(String fileName){
        this.fileName= fileName;
    }

    @Override
    public void play() {
        System.out.println("Play black and white video " + fileName);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String videoName) {
        this.fileName = videoName;
    }
}
