package isp.lab5.exercise4;

public class Exercise4 {
    public static class Main {
        public void main(String[] args) {
            AbstractShapeFactory roundedShapeFactory = ShapeFactoryProvider.getShapeFactory("rounded");
            AbstractShapeFactory normalShapeFactory = ShapeFactoryProvider.getShapeFactory("normal");

            // create instances
            Shape rectangle = roundedShapeFactory.getShape("roundedRectangle");
            Shape shape = normalShapeFactory.getShape("rectangle");
            Rectangle r1 = new Rectangle();
            RoundedRectangle r2 = new RoundedRectangle();

            r1.draw();
            r2.draw();
            rectangle.draw();
            shape.draw();
        }
    }
}
