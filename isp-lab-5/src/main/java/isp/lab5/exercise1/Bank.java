package isp.lab5.exercise1;

import java.util.ArrayList;

public class Bank {

    private ArrayList<Account> accounts = new ArrayList<Account>(11);

    public String executeTransaction(Transaction t){
        return t.execute();
    }

    public Account getAccountByCardId(String cardId){
        Account toReturn;
        for(Account curr_acc : accounts) {
            for (Card curr_card: curr_acc.getCards()) {
                if(curr_card.getCardId().equals(cardId)) {
                    toReturn = curr_acc;
                    return toReturn;
                }
            }
        }

        return null;
    }
}
