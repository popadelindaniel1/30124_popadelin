package isp.lab5.exercise1;

public class ATM {

    Bank bank;
    Card card;

    public ATM(Bank bank) {
        this.bank = bank;

    }

    public ATM(Bank bank, Card card) {
        this.bank = bank;
        this.card = card;

    }

    public void changePin(String oldPin, String newPin){
        ChangePin cp = new ChangePin(oldPin, newPin);
        bank.executeTransaction(cp);

    }

    public void withdraw(double amount){
        WithdrawMoney wm = new WithdrawMoney(amount);
        bank.executeTransaction(wm);
    }

    public void checkMoney(){
        CheckMoney cm = new CheckMoney();
        bank.executeTransaction(cm);
    }

    public boolean insertCard(Card card, String pin){
        if(card.getPin().equals(pin)) {
            this.card = card;
            return true;
        }

        return false;
    }

    public void removeCard(){

        this.card = null;
    }
}
