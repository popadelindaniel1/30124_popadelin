package isp.lab5.exercise1;

import java.util.ArrayList;

public class Account {
    private String name;
    private double balance;
    private ArrayList<Card> cards = new ArrayList<Card>();

    public Account(){

    }

    public Account(double balance){
        this.balance = balance;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return this.balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }
}
