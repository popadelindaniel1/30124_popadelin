package isp.lab5.exercise1;

public class WithdrawMoney extends Transaction{
    public double amount;

    public WithdrawMoney(double amount) {
        this.amount = amount;
    }

    public String execute(){
        double newBalance = account.getBalance() - amount;
        account.setBalance(newBalance);
        return Double.toString(amount);
    }
}
