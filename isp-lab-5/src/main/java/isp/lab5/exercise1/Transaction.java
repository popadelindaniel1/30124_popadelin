package isp.lab5.exercise1;

public abstract class Transaction {

    Account account = new Account();

    public abstract String execute();
}
