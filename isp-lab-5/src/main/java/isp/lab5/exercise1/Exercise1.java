package isp.lab5.exercise1;

import java.util.Scanner;


public class Exercise1 {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        Bank bank = new Bank();
        ATM atm = new ATM(bank);
        Card card = new Card("345XB", "2345X");
        Account account = new Account(20000.3);
        WithdrawMoney withdrawMoney = new WithdrawMoney(account.getBalance());

        System.out.println("Insert the card and introduce the pin: ");
        String pin = keyboard.next();

        while(!card.getPin().equals(pin)){
            System.out.println("Wrong pin! ");
            pin = keyboard.next();
        }

        atm.insertCard(card, pin);

        System.out.println("What option do you choose? " +
                "1. Change Pin " +
                "2. Withdraw " +
                "3. Check Money " +
                "4. Remove Card ");
        int choice = keyboard.nextInt();

        if(choice == 1) {
            System.out.println("Write the new pin: ");
            String newPin = keyboard.next();
            atm.changePin(pin, newPin);
            System.out.println("You changed the pin: ");
        }
        else if(choice == 2){
            System.out.println("Insert the amount: ");
            double amount = keyboard.nextDouble();
            atm.withdraw(amount);
            System.out.println("You withdrew some money!");
        }
        else if(choice == 3){
            atm.checkMoney();
        }

        else if(choice == 4){
            atm.removeCard();
            System.out.println("Card was removed");
        }
        else {
            System.out.println("Wrong choice! You need to select another option");
        }
    }
}
