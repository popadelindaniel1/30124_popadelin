package tema3;

import ex1.Tree;
import org.junit.Test;


public class ex1Test {

    @Test
    public void test1() {
        Tree a = new Tree();
        a.grow(5);

        assert (a.getHeight() == 20);
    }
}
