package tema3;

import ex2.Rectangle;
import org.junit.Test;


public class ex2Test {


    @Test
    public void ex2TestPer() {
        Rectangle a=new Rectangle();
        int peri=6;
        assert (peri == a.getPerimeter());
    }
    @Test
    public void ex2TestAr() {
        Rectangle a=new Rectangle();
        int ar=2;
        assert (ar == a.getArea());
    }

}
