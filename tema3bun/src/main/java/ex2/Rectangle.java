package ex2;

public class Rectangle {
    private int length;
    private int width;
    private String color;

    public Rectangle()
    {
        this.length=2;
        this.width=1;
        this.color="Red";
    }

    public Rectangle(int length,int width)
    {
        this.length=length;
        this.width=width;
    }
    public Rectangle(int length,int width,String color)
    {
        this.width=width;
        this.length=length;
        this.color=color;
    }

    public int getLength() {
        return length;

    }

    public int getWidth() {
        return width;
    }

    public String getColor() {
        return color;
    }
    public int getPerimeter()
    {
        return 2*(length+width);
    }
    public int getArea(){
        return length*width;

    }
}
