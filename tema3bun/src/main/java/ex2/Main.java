package ex2;

public class Main {
    public static void main(String[] args) {
        Rectangle a=new Rectangle();
        Rectangle b=new Rectangle(4,2,"Blue");
        System.out.println("Lungimea: " + a.getLength());
        System.out.println("Inaltimea: "+ a.getWidth());
        System.out.println("Culoarea: "+ a.getColor());
        System.out.println("Aria: "+ a.getArea());
        System.out.println("Perimetrul:"+ a.getPerimeter());

        System.out.println();
        System.out.println("Lungimea: " + b.getLength());
        System.out.println("Inaltimea: "+ b.getWidth());
        System.out.println("Culoarea: "+ b.getColor());
        System.out.println("Aria: "+ b.getArea());
        System.out.println("Perimetrul:"+ b.getPerimeter());
    }

}
