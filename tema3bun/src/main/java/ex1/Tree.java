package ex1;

public class Tree {
    private int height;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Tree()
    {
        this.height=15;
    }

   @Override
    public String toString() {
        return "height=" + height ;
    }


    public void grow(int meters)
    {
        if(meters>=1)
        {
            this.height=this.height+meters;
        }
    }
}
