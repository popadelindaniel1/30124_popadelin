package ex5;

public class Item {
    private String name;
    private double price;
    private int quantity;

    public Item(String name,double price,int quantity)
    {
        this.name=name;
        this.price=price;
        this.quantity=quantity;
    }
    public String getName()
    {
        return this.name;
    }
    public double getPrice()
    {
        return this.price;
    }

    public int getQuantity() {
        return this.quantity;
    }
    public void reduceQuantity()
    {
        if(quantity > 0)
            quantity=quantity-1;
    }
}