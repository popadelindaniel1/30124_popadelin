package ex5;

import java.util.Scanner;

public class VendingMachine {
    private Item[] items = new Item[5];

    public VendingMachine() {
        items[0] = new Item("Fanta", 6.0, 10);
        items[1] = new Item("Coca-Cola", 6.0, 10);
        items[2] = new Item("Lays", 7.0, 20);
        items[3] = new Item("7Days", 4.0, 5);
        items[4] = new Item("Eugenia", 2.0, 20);

    }

    public void displayProducts() {
        for (int i = 0; i < items.length; i++) {
            System.out.println("Produs: " + items[i].getName());
            System.out.println("Pret: " + items[i].getPrice() + " lei");
        }
    }

    public void dispenseItem(int id) {
        Scanner scanner = new Scanner(System.in);

        if (items[id].getQuantity() <= 0) {
            System.out.println("Stoc epuizat!");
        } else {

            System.out.println("Pret:" + items[id].getPrice());
            System.out.println("Introdu banii:");
            double amt = scanner.nextDouble();
            if (amt < items[id].getPrice()) {
                System.out.println("Bani insuficienti! " + items[id].getName());
                System.out.println("Rambursare: " + amt);
            } else {
                System.out.println("Dispensing one " + items[id].getName());
                double changeAmt = amt - items[id].getPrice();
                if (changeAmt > 0)
                    System.out.println("Here is your change amount of " + changeAmt);
                items[id].reduceQuantity();
            }
        }
    }

    Item[] getItems() {
        return items;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        VendingMachine vm = new VendingMachine();
        Item[] vmItems = vm.getItems();
        vm.displayProducts();
        System.out.println("Vending Machine Menu");
        for (int i = 0; i < vmItems.length; i++) {
            System.out.println("Enter " + (i+1) + " for " + vmItems[i].getName());
        }

        System.out.println("Enter 6 to stop the Vending Machine");

        int choice;
        do {
            System.out.print("Enter your choice: ");
            choice = in.nextInt();

            if (choice < 1 || choice > 6) {
                System.out.println("Incorrect choice");
            }
            else if(choice == 6) {
                System.out.println("Stopping Vending Machine");
            }
            else {
                vm.dispenseItem(choice - 1);
            }
        } while(choice != 6);



    }

}
