package ex4;

public class MyPoint {
    private int x;
    private int y;
    private int z;

    public MyPoint()
    {
        this.x=0;
        this.y=0;
        this.z=0;
    }
    public MyPoint(int x,int y,int z)
    {
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
    public void setXYZ(int x,int y,int z)
    {
        this.x=x;
        this.y=y;
        this.z=z;
    }

    @Override
    public String toString() {
        return "(x,y,z)" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
    public double distance(int x, int y, int z)
    {
        double d=Math.pow((Math.pow(this.x-x,2)+
                Math.pow(this.y-y,2)+
                Math.pow(this.z-z,2)*
                        1.0),0.5);
        return d;
    }


}
