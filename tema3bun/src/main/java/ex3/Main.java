package ex3;

public class Main {
    public static void main(String[] args) {
        Vehicle a=new Vehicle("Audi",160,"A4",'D');
        Vehicle b=new Vehicle("BMW",170,"X5",'B');

        b.setModel("Mercedes");
        b.setSpeed(190);
        b.setType("a class");

        System.out.println("Model: ");
        System.out.println(b.getModel());
        System.out.println("Tip: ");
        System.out.println(b.getType());
        System.out.println("Viteza: ");
        System.out.println(b.getSpeed());
        System.out.println("Combustibil: ");
        System.out.println(b.getFuelType());

        System.out.println(a.equals(b));

        System.out.println(a.toString());
        System.out.println(b.toString());
    }

}
