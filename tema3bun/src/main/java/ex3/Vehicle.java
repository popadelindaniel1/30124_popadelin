package ex3;

import ex1.Tree;

public class Vehicle{
    private String model;
    private int speed;
    private String type;
    private char fuelType;

    public Vehicle(String model,int speed,String type,char fuelType)
    {
        this.model=model;
        this.speed=speed;
        this.type=type;
        this.fuelType=fuelType;

    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSpeed() {
        return speed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public char getFuelType() {
        return fuelType;
    }

    public void setFuelType(char fuelType) {
        this.fuelType = fuelType;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "model='" + model + '\'' +
                ", speed=" + speed +
                ", type='" + type + '\'' +
                ", fuelType=" + fuelType +
                '}';
    }

    public boolean equals(Vehicle vehicle)
    {
        if(this.speed==vehicle.getSpeed() && this.fuelType==vehicle.getFuelType())
            return true;
        return false;
    }

}
