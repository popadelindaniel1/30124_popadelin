package isp.lab4.exercise5;

public class Controler {
    private final  TemperatureSensor[] temperatureSensors=new TemperatureSensor[3];
    private final FireAlarm fireSensor;

    public Controler()
    {
        temperatureSensors[0]=new TemperatureSensor(80,"Cluj");
        temperatureSensors[1]=new TemperatureSensor(50,"Suceava");
        temperatureSensors[2]=new TemperatureSensor(50,"Timis");
        fireSensor=new FireAlarm(true);

    }

    public void controlStep() {
        int k = 0;
        for (TemperatureSensor temperatureSensor : temperatureSensors) {
            if (temperatureSensor.getValue() > 50) {
                k++;
                System.out.println("Alarma de incendiu a inceput la: " + temperatureSensor.getLocation());
            } else {
                System.out.println("Alarma de incendiu nu a inceput: " + temperatureSensor.getLocation());
            }
        }
        if (k > 0)

            System.out.println("Senzorul de incendiu: " + fireSensor.isActive());

    }
}
