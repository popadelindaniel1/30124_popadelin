package isp.lab4.exercise6;

import java.util.Scanner;

public class SalariedEmployee extends Employee{
    private double weeklySalary;

    Scanner scanner=new Scanner(System.in);

    public SalariedEmployee(String firstName,String lastName)
    {
        super(firstName,lastName);

    }
    public SalariedEmployee(double weeklySalary,String firstName,String lastName)
    {
        super(firstName,lastName);
        this.weeklySalary=weeklySalary;
    }

    @Override
    public double getPaymentAmount() {
        return weeklySalary;

    }
}
