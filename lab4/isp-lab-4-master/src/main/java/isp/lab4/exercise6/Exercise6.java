package isp.lab4.exercise6;

import java.util.Random;

public class Exercise6 {
    public static void main(String[] args) {
        HourlyEmployee hourlyEmployee=new HourlyEmployee(50,40,"Popescu","Alin");
        ComissionEmployee employee1=new ComissionEmployee(300,0.5,"Popescu","Marius");
        SalariedEmployee salariedEmployee=new SalariedEmployee("Popescu","Sara");

        System.out.println(hourlyEmployee.getPaymentAmount());
        System.out.println(employee1.getPaymentAmount(20));
        System.out.println(salariedEmployee.getPaymentAmount());


        Employee[] employees=new Employee[6];

        employees[0]=new HourlyEmployee(60,50,"Valcan","Andrei");
        employees[1]=new HourlyEmployee(20,40,"Gan","Alex");
        employees[2]=new ComissionEmployee(150,0.5,"Aul","Adi");
        employees[3]=new ComissionEmployee(100,0.6,"Dumitrescu","Mircea");
        employees[4]=new SalariedEmployee(30,"Alba","Alin");
        employees[5]=new SalariedEmployee(105,"Aris","Adina");



        for(int i=0;i<employees.length;i++)
        {   double k;
            k=employees[i].getPaymentAmount();
            System.out.println(k);

        }


    }
}
