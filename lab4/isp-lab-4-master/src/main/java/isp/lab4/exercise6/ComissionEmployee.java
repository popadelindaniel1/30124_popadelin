package isp.lab4.exercise6;

public class ComissionEmployee extends Employee{
    private double grossSales;
    private double comissionSales;

    public ComissionEmployee(double grossSales,double comissionSales,String firstName,String lastName)
    {
        super(firstName,lastName);
        this.grossSales=grossSales;
        this.comissionSales=comissionSales;

        if(grossSales < 0)
            System.out.println("Gross sales must be >= 0");

        if(comissionSales <= 0 || comissionSales >= 1)
            System.out.println("Comission sales must be 0 and < 1 ");

    }
    public void setGrossSales(double grossSales)
    {
        if(grossSales<0)
            System.out.println("Gross sales must be >= 0");
        this.grossSales=grossSales;
    }
    public double getGrossSales() {
        return grossSales;
    }
    public void setComissionSales(double comissionSales)
    {
        if(comissionSales <= 0 || comissionSales >= 1)
            System.out.println("Comission rate must be > 0 and <1");
        this.comissionSales=comissionSales;
    }

    public double getComissionSales() {
        return comissionSales;
    }
    public double getPaymentAmount(double d)
    {
        return (grossSales+(comissionSales * grossSales));
    }


}
