package isp.lab4.exercise4;

public class FireAlarm {
    private boolean active;

    public FireAlarm()
    {

    }

    public FireAlarm(boolean active)
    {
        this.active=active;
    }
    public boolean isActive()
    {
        return true;
    }
    public boolean setActive(boolean active)
    {
        if(isActive())
            return true;
        else
            return false;

    }

    @Override
    public String toString() {
        return "FireAlarm{" +
                "active=" + active +
                '}';
    }
}
