package isp.lab4.exercise4;

import java.util.Scanner;

public class TemperatureSensor {
    Scanner scanner=new Scanner(System.in);
    private int value;
    private String location;

    public TemperatureSensor()
    {
        System.out.println("Introdu valoarea temperaturii: ");
        value=scanner.nextInt();

    }
    public TemperatureSensor(int type,String location)
    {
        this.value=type;
        this.location=location;
    }

    public int getValue() {
        return value;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "TemperatureSensor{" +
                "value=" + value +
                ", location='" + location + '\'' +
                '}';
    }
}
