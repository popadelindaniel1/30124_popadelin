package isp.lab4.exercise4;


import java.util.Scanner;

public class Controler {
    Scanner scanner=new Scanner(System.in);
    private TemperatureSensor[] temperatureSensors;
    private FireAlarm fireAlarm;

    public Controler(TemperatureSensor temperatureSensor[],FireAlarm fireAlarm)
    {
        this.fireAlarm=fireAlarm;
        this.temperatureSensors=temperatureSensor;
    }

    public void controlStep()
    {
        int k=0;
        for(int i=0;i<temperatureSensors.length;i++) {
            if (temperatureSensors[i].getValue() > 50) {
                k++;
                System.out.println();
                System.out.println("Alarma de incendiu a inceput la " +temperatureSensors[i].getLocation());

            } else {
                System.out.println("Alarma de incendiu nu a inceput la " +temperatureSensors[i].getLocation());
            }

        }
        if(k==3)
            System.out.println("Senzorul de alarma: true");
        else System.out.println("Senzorul de alarma: false");
    }



}
