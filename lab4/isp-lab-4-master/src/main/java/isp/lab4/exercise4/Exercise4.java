package isp.lab4.exercise4;


public class Exercise4 {
    public static void main(String[] args) {

        TemperatureSensor[] temperatureSensors = new TemperatureSensor[3];
        FireAlarm fireAlarm = new FireAlarm();

        temperatureSensors[0]=new TemperatureSensor(80,"Cluj");
        temperatureSensors[1]=new TemperatureSensor(70,"Suceava");
        temperatureSensors[2]=new TemperatureSensor(90,"Timis");

        Controler controler=new Controler(temperatureSensors,fireAlarm);

        controler.controlStep();

    }
}
