package isp.lab4.exercise3;

import java.util.Scanner;

public class TemperatureSensor {
    Scanner scanner=new Scanner(System.in);
    //atribute
    private int value;
    private String location;
    //constructori
    public TemperatureSensor(String location)
    {
        this.location=location;
        System.out.println("Introdu valoarea temperaturii! ");
        value=scanner.nextInt();

    }
    public TemperatureSensor(int type,String location)
    {
        value=type;
        this.location=location;
    }
    //metode

    public int getValue() {
        return value;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "TemperatureSensor{" +
                "value=" + value +
                ", location='" + location + '\'' +
                '}';
    }
}
