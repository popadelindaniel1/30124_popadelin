package isp.lab4.exercise3;

public class Exercise3 {
    public static void main(String[] args) {
        TemperatureSensor t = new TemperatureSensor(53,"CLuj");
        TemperatureSensor t2=new TemperatureSensor(20,"Timisoara");
        TemperatureSensor t3=new TemperatureSensor("Suceava");
        FireAlarm f = new FireAlarm(true);

        Controler controler = new Controler(t, f);
        Controler controler2=new Controler(t2,f);
        Controler controler3=new Controler(t3,f);
        controler3.controlStep();
        System.out.println();
        controler.controlStep();
        System.out.println();
        controler2.controlStep();
    }
}
