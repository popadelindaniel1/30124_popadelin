package isp.lab4.exercise3;

public class Controler {
    private TemperatureSensor temperatureSensor;
    private FireAlarm fireAlarm;

    public Controler(TemperatureSensor temperatureSensor, FireAlarm fireAlarm)
    {
        this.temperatureSensor=temperatureSensor;
        this.fireAlarm=fireAlarm;
    }
    public void controlStep()
    {
        if(temperatureSensor.getValue() > 50)
        {
            fireAlarm.setActive(true);
            System.out.println("Alarma de incendiu la " + temperatureSensor.getLocation() + " a inceput!");
        }else
        {
            System.out.println("Alarma de incendiu la " + temperatureSensor.getLocation() + " oprita!");
        }
    }


}
