package isp.lab4.exercise1;

import isp.lab4.exercise6.ComissionEmployee;
import isp.lab4.exercise6.HourlyEmployee;
import isp.lab4.exercise6.SalariedEmployee;
import org.junit.Assert;
import org.junit.Test;

public class Test6 {
    @Test
    public void HourlyEmployee() {
        HourlyEmployee hourlyEmployee=new HourlyEmployee(50,40,"Popescu","Alin");
        int expected=2000;
        Assert.assertEquals(expected,hourlyEmployee.getPaymentAmount(),0);
        ComissionEmployee employee1=new ComissionEmployee(300,0.5,"Popescu","Marius");
        SalariedEmployee salariedEmployee=new SalariedEmployee(100,"Popescu","Sara");
        expected=100;
        //Assert.assertEquals(expected,hourlyEmployee.getPaymentAmount()+employee1.getPaymentAmount()+ salariedEmployee.getPaymentAmount(),1e-3);
        Assert.assertEquals(expected,salariedEmployee.getPaymentAmount(),1e-3);
        expected=450;
        Assert.assertEquals(expected,employee1.getPaymentAmount(20),1e-3);

    }
}
