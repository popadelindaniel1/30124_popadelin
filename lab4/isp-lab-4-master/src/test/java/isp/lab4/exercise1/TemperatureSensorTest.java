package isp.lab4.exercise1;

import org.junit.Assert;
import org.junit.Test;

public class TemperatureSensorTest {

    @Test
    public void Test1()
{
    TemperatureSensor temp1=new TemperatureSensor(20,"Cluj");
    int expected=20;
    Assert.assertEquals(expected,temp1.getValue());

    TemperatureSensor temp2=new TemperatureSensor();
    expected=0;
    Assert.assertEquals(expected,temp2.getValue());

    String expect1="Cluj";
    Assert.assertEquals(expect1,temp1.getLocation());

    expect1="x";
    Assert.assertEquals(expect1,temp2.getLocation());

}


}
