package isp.lab7.safehome;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DoorLockController implements ControllerInterface {

    private Map<Tenant,AccessKey>validAccess;
    private Door door;
    private List<Accesslog>accessLogs;
    int max_retries=3;
    int current_retries=0;

    public DoorLockController(Door door, List<Accesslog> accessLogs) {
        this.door = door;
        this.accessLogs=accessLogs;
        this.validAccess = new HashMap<>();
        this.validAccess.put(new Tenant(ControllerInterface.MASTER_TENANT_NAME), new AccessKey(ControllerInterface.MASTER_KEY));
    }

    private boolean PermanentlyLocked()
    {
        return current_retries>=max_retries;
    }

    private Accesslog createAccessLog(String tenant,String operation,String error)
    {
        return new Accesslog(tenant, LocalDateTime.now(),operation,door.getDoorStatus(),error);
    }



    @Override
    public DoorStatus enterPin(String pin) throws Exception {
        Map.Entry<Tenant, AccessKey> accessKeyEntry = validAccess.entrySet().stream().filter(tenantAccessKeyEntry -> tenantAccessKeyEntry.getValue().getPin().equals(pin)).findFirst().orElse(null);
        if (Objects.isNull(accessKeyEntry)) {
            current_retries++;
            if (accessKeyEntry.getKey().getName().equals(ControllerInterface.MASTER_TENANT_NAME)) {
                current_retries = 0;
            } else if (PermanentlyLocked()) {
                accessLogs.add(createAccessLog("", "ENTER PIN OPERATION", "TooManyAttemptsException"));
                throw new TooManyAttemptsException();
            }
            if (PermanentlyLocked()) {
                current_retries = 3;
                accessLogs.add(createAccessLog("", "ENTER PIN OPERATION", "TooManyAttemptsException"));
                throw new TooManyAttemptsException();
            } else {
                accessLogs.add(createAccessLog("", "ENTER PIN OPERATION", "InvalidPinException"));
                throw new InvalidPinException();
            }
            if (door.getDoorStatus() == DoorStatus.OPEN) {
                this.door.lockDoor();
            } else {
                this.door.unlockDoor();
            }
            accessLogs.add(createAccessLog("", "ENTER PIN", ""));
            return door.getDoorStatus();
        }
    }

        @Override
        public void addTenant (String pin, String name) throws Exception {
            Tenant tenant = new Tenant(name);

            if (validAccess.containsKey(tenant)) {
                accessLogs.add(createAccessLog(name, "ENTER PIN", ""));
                throw new TenantAlreadyExistsException();
            }
            accessLogs.add(createAccessLog(name, "ENTER PIN", ""));
            validAccess.put(tenant, new AccessKey(pin));

        }

        @Override
        public void removeTenant (String name) throws Exception {
            Tenant tenant = new Tenant(name);

            if (!validAccess.containsKey(tenant)) {
                accessLogs.add(createAccessLog(name, "ADD OPERATION", "NotFoundException"));
                throw new TenantNotFoundException();
            }
            accessLogs.add(createAccessLog(name, "ADD OPERATION", ""));
            validAccess.remove(tenant);

        }

}
