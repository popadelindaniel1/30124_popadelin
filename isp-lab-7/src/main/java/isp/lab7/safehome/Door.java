package isp.lab7.safehome;

public class Door {
    private DoorStatus doorStatus;

    public Door()
    {
        this.doorStatus=DoorStatus.CLOSE;
    }
    public DoorStatus getDoorStatus()
    {
        return doorStatus;
    }
    public void lockDoor()
    {
        this.doorStatus=DoorStatus.CLOSE;
    }
    public void unlockDoor()
    {
        this.doorStatus=DoorStatus.OPEN;
    }
}
