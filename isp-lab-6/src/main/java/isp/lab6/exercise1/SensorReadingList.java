package isp.lab6.exercise1;

import isp.lab6.ex3.SensorReading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SensorReadingList implements IReadingRepository{
    ArrayList<SensorReading>readings=new ArrayList<>();

    public void addReading(SensorReading reading){
        readings.add(reading);
    }

    @Override
    public double getAvarageValueByType(Type type, String location) {
        return 0;
    }

    @Override
    public List<isp.lab6.exercise1.SensorReading> getReadingsByType(Type type) {
        return null;
    }


    @Override
    public void listSortedByLocation() {

    }

    @Override
    public void listSortedByValue() {

    }

    @Override
    public List<isp.lab6.exercise1.SensorReading> findAllByLocationAndType(String location, Type type) {
        return null;
    }

}
