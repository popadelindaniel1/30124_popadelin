package isp.lab6.exercise1;

import isp.lab6.ex3.SensorReading;

import java.util.List;

public enum Type {
    TEMPERATURE, HUMIDITY, PRESSURE;

}
