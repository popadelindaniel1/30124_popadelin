package isp.lab6.ex3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Sensor {
    private final SENSOR_TYPE type;
    private final String id;
    private List<SensorReading>sensorReadings;


    public List<SensorReading>getSensorReadings()
    {
        return sensorReadings;
    }
    public Sensor(SENSOR_TYPE type,String id)
    {
        this.type=type;
        this.id=id;
    }

    public String getId()
    {
        return id;
    }

    public List<SensorReading> getSensorReadingSortedByDateAndTime()
    {if(sensorReadings != null) {
        Collections.sort(this.sensorReadings);
        return this.sensorReadings;
    }
    return null;
    }
    public List<SensorReading> getSensorReadingsSortedByValue()
    {
        if(sensorReadings != null)
        {
            this.sensorReadings.sort(new SensorReading.ValueCompar());
        }
        return null;
    }
    public boolean addSensorReading(SensorReading sr)
    {if(getSensorReadings()==null)
    {
        this.sensorReadings=new ArrayList<>();
    }
        this.sensorReadings.add(sr);
        return true;
    }


}
