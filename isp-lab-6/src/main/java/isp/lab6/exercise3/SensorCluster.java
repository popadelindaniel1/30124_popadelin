package isp.lab6.ex3;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SensorCluster {
    private List<Sensor> sensorList;

    public SensorCluster(List<Sensor>sensorList)
    {
        this.sensorList=sensorList;
    }

    public List<Sensor> getSensorList() {
        return sensorList;
    }

    public Sensor addSensor(String sensorId, SENSOR_TYPE type)
    {
        if (this.sensorList == null) {
            sensorList = new ArrayList<>();
        }
        for (Sensor sensor : sensorList) {
            if (sensor.getId().equals(sensorId)) {
                return null;
            }
        }
       Sensor sensor=new Sensor(type,sensorId);
       this.sensorList.add(sensor);
       return sensor;
    }

    public Sensor getSensorById(String sensorId)
    {
        for(Sensor sensor:sensorList)
            if(sensor.getId().equals(sensorId))
            {
                return sensor;
            }
        return null;
    }

    public boolean writeSensorReading(String sensorId, double value, LocalDateTime dateTime)
    {
        if(sensorList!=null)
        {
            Sensor sensor=getSensorById(sensorId);
            if(sensor != null)
            {
                return sensor.addSensorReading(new SensorReading(value,dateTime));
            }
        }
        return false;
    }


}
