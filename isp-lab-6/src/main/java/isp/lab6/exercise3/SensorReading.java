package isp.lab6.ex3;

import java.time.LocalDateTime;
import java.util.Comparator;

public class SensorReading implements Comparable<SensorReading>{

    private final double value;
    private final LocalDateTime dateAndTime;

    public SensorReading(double value,LocalDateTime dateAndTime)
    {
        this.value=value;
        this.dateAndTime=dateAndTime;
    }

    public LocalDateTime getDateAndTime()
    {
        return dateAndTime;
    }

    public double getValue() {
        return value;
    }

    @Override
    public int compareTo(SensorReading sensorReading) {
        return this.getDateAndTime().compareTo(sensorReading.getDateAndTime());
    }
    public static class ValueCompar implements Comparator<SensorReading> {

        @Override
        public int compare(SensorReading sensorReading1, SensorReading sensorReading2) {
            if (sensorReading1.getValue() > sensorReading2.getValue()) {
                return 1;
            } else if (sensorReading1.getValue() < sensorReading2.getValue()) {
                return -1;
            }
            return 0;
        }
    }
}
