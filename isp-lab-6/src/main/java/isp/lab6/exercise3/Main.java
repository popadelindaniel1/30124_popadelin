package isp.lab6.ex3;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        SensorCluster sensorCluster=new SensorCluster(new ArrayList<>());

        sensorCluster.addSensor("0",SENSOR_TYPE.TEMPERATURE);
        sensorCluster.addSensor("1",SENSOR_TYPE.HUMIDITY);
        sensorCluster.addSensor("2",SENSOR_TYPE.PRESSURE);

        if(sensorCluster.writeSensorReading("1",20, LocalDateTime.now()));
        {
            System.out.println("Citire realizata cu succes!");
        }
        if(sensorCluster.writeSensorReading("1",21,LocalDateTime.now().plusMinutes(1))){
            System.out.println("Citire realizata cu succes!");;
        }
        if(sensorCluster.writeSensorReading("1",22,LocalDateTime.now().plusMinutes(2))){
            System.out.println("Citire realizata cu succes!");;
        }

        sensorCluster.writeSensorReading("2",30,LocalDateTime.now());
        sensorCluster.writeSensorReading("2",31,LocalDateTime.now().plusMinutes(1));
        sensorCluster.writeSensorReading("2",32,LocalDateTime.now().plusMinutes(2));

        Sensor sensor=sensorCluster.getSensorById("1");
        Sensor sensor1=sensorCluster.getSensorById("2");
        System.out.println(sensor);
        System.out.println(sensor1);

        System.out.println(sensor.getSensorReadingSortedByDateAndTime());
        System.out.println(sensor.getSensorReadingsSortedByValue());

        System.out.println(sensor1.getSensorReadingSortedByDateAndTime());
        System.out.println(sensor1.getSensorReadingsSortedByValue());
    }

}
