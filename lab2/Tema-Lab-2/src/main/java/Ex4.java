public class Ex4 {

    /**
     * This method should verify if a number is prime
     *
     * @param number the number to check
     * @return true if number is prime and false otherwise
     */
    public static boolean isPrimeNumber(final int number) {
        int n=number;
        if(n<=1)
            return false;
        if(n==2)
            return true;
        if(n%2==0)
            return false;
        for(int i=3;i<=Math.sqrt(n);i+=2)
        {
            if(n%i==0)
                return false;
        }
        return true;
    }

    /**
     * This method should get the first(by position) odd number from the given array
     *
     * @param someNumbers the given int array
     * @return first odd number from the array
     */
    public static int firstOdd(int[] someNumbers) {
        for (int i=0;i< someNumbers.length;i++)
            if(someNumbers[i]%2!=0)
            {
                return someNumbers[i];
            }
        return 0;
    }

    /**
     * This method should get the first(by position) even number from the given array
     *
     * @param someNumbers the given int array
     * @return first even number from the array
     */
    public static int firstEven(int[] someNumbers) {
        for (int i=0;i< someNumbers.length;i++)
            if(someNumbers[i]%2==0)
            {
                return someNumbers[i];
            }
        return 0;
    }

    /**
     * This method should get the first(by position) prime number from the given array
     *
     * @param someNumbers the given int array
     * @return first prime number from the array
     */
    public static int firstPrime(int[] someNumbers) {
        // TODO: Use isPrimeNumber(final int number)
        for (int i=0;i< someNumbers.length;i++)
            if(isPrimeNumber(someNumbers[i])==true)
            {
                return someNumbers[i];
            }
        return 0;
    }

    /**
     * Don't forget to also display the position of each number
     */
    public static void main(String[] args) {
        int[] someNumbers = new int[]{15, 18, 13, 22, 21, 11, 57, 141, 563, 16};

        int ip=0,io=0,ie=0;
        for(int i=0;i<someNumbers.length;i++)
        {
            if(someNumbers[i]==firstPrime(someNumbers) && ip==0)
                ip=i;
            if(someNumbers[i]==firstOdd(someNumbers) && io==0)
                io=i;
            if(someNumbers[i]==firstEven(someNumbers) && ie==0)
                ie=i;
        }
        System.out.println("First odd number: " + firstOdd(someNumbers)+" at position "+io);
        System.out.println("First even number: " + firstEven(someNumbers)+" at position "+ie);
        System.out.println("First prime number: " + firstPrime(someNumbers)+" at position "+ip);
    }
}