public class Ex5 {

    /**
     * This method should generate an array that have 20 random nubers between -1000 and 1000
     *
     * @return the random numbers
     */
    public static int[] generateRandomNumbers() {
        int max=1000,min=-1000,r;
        int vec[]=new int[20];
        for(int i=0;i<20;i++)
        {
            r=(int) (Math.random() * (max-min))+min;
            vec[i]=r;
        }
        System.out.println(vec[2]);
        return vec;
    }

    /**
     * This method should sort the given random numbers
     *
     * @param randomNumbers numbers generated random
     * @return sorted int array
     */
    public static int[] getSortedNumbers(int[] randomNumbers) {
        int OK=0;
        do{
            OK=1;
            for(int i=0;i<19;i++)
            {
                if(randomNumbers[i]>randomNumbers[i+1])
                {
                    int aux=randomNumbers[i+1];
                    randomNumbers[i+1]=randomNumbers[i];
                    randomNumbers[i]=aux;
                    OK=0;
                }
            }
        }while(OK!=1);
        return randomNumbers;
    }

    public static void main(String[] args) {
        // display the random generated numbers
        int[] randomNumbers = generateRandomNumbers();
        System.out.println("The random generated numbers are:");
        for (int i = 0; i < randomNumbers.length; i++) {
            System.out.print(randomNumbers[i] + ", ");
        }
        int[] sortedNumbers = getSortedNumbers(randomNumbers);
        // display the sorted numbers
        System.out.println("\nThe sorted numbers are:");
        for (int i = 0; i < sortedNumbers.length; i++) {
            System.out.print(sortedNumbers[i] + ", ");
        }
    }
}