public class Ex3 {

    /**
     * This method should verify if a number is prime
     *
     * @param number the number to check
     * @return true if number is prime and false otherwise
     */
    public static boolean isPrimeNumber(final int number) {
        int n=number;
        if(n<=1)
            return false;
        if(n==2)
            return true;
        if(n%2==0)
            return false;
        for(int i=3;i<=Math.sqrt(n);i+=2)
        {
            if(n%i==0)
                return false;
        }
        return true;
    }

    /**
     * This method should calculate the sum of digits of a given number
     *
     * @param number the number used to calculate the sum of digits
     * @return an int representing the sum of digits of the given number
     */
    public static int calculateSumOfDigits(int number) {
        int n=number;
        int nr=0;
        while(n!=0){
            nr+=n%10;
            n/=10;
        }
        return nr;
    }

    /**
     * This method should extract the prime numbers from a given interval
     * using isPrimeNumber(final int number) method defined above
     * NOTE* a < b
     *
     * @param a the left end of the interval
     * @param b the right end of the interval
     * @return and int array consisting of the prime numbers from the given interval
     */
    public static int[] getPrimeNumbersFromInterval(int a, int b) {
        // TODO: Use isPrimeNumber(final int number)
        int array[]=new int[b-a],k=0;
        for(int i=a;i<=b;i++)
            if(isPrimeNumber(i)==true)
                array[k++]=i;
        int prime[]=new int[k];
        for(int i=0;i<k;i++)
            prime[i]=array[i];
        return prime;
    }

    /**
     * This method should calculate the geometric mean of the given prime numbers
     *
     * @param primeNumbers the numbers used to calculate the geometric mean
     * @return the geometric mean of the given numbers
     */
    public static double calculateGeometricMean(int[] primeNumbers) {
        int k=0;
        double prod=1;
        for(int i=0;i< primeNumbers.length;i++)
            {
                k++;
                prod *= primeNumbers[i];
            }
        double result=Math.pow(prod,(double)1/k);
        return result;
    }

    /**
     * This method should calculate the number of prime numbers which
     * have the sum of digits an even number
     * NOTE* use calculateSumOfDigits(int number)
     *
     * @param primeNumbers prime numbers used for calculation
     * @return the numbers which respect the given condition
     */
    public static int numberOfPNWithEvenSumOfDigits(int[] primeNumbers) {
        // TODO: Use calculateSumOfDigits(int number)
        int nr=0;
        for(int i=0;i< primeNumbers.length;i++)
        {
            if(calculateSumOfDigits(primeNumbers[i])%2==0)
                nr++;
        }
        return nr;
    }

    public static void main(String[] args) {
        int a = 1;
        int b = 20;
        System.out.println("The geometric mean is: " + calculateGeometricMean(getPrimeNumbersFromInterval(a, b)));
        System.out.println("The number of prime numbers which have the sum of digits an even number is: " + numberOfPNWithEvenSumOfDigits(getPrimeNumbersFromInterval(a, b)));
    }
}