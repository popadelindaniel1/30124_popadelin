import java.util.Scanner;

public class Ex1 {
    /**
     * This method should generate an random number between 2 and 10 then
     * should ask user to enter that amount of numbers from keyboard and
     * store the numbers in an int array which should be returned
     *
     * @return the array of numbers read from keyboard
     */
    private static int[] getUserNumbers() {
        int min=2;
        int max=10;
        int r=(int) (Math.random() * (max-min))+min;

        Scanner scan=new Scanner(System.in);
        int array[]=new int[r];
        System.out.println("Inserati "+r+" elemente pentru vector: ");
        for(int i=0;i<r;i++)
            array[i] = scan.nextInt();
        return array;
    }

    /**
     * This method should compute the arithmetical mean of the given numbers
     *
     * @param userNumbers the numbers used to calculate the arithmetical mean
     * @return the arithmetical mean of the given numbers
     */
    protected static double computeTheArithmeticalMean(int[] userNumbers) {
        double sum=0;
        int n=userNumbers.length;
        for(int i=0;i< n;i++)
        {
            sum+=userNumbers[i];
        }
        return (double)sum/n;
    }

    public static void main(String[] args) {
        int[] userNumbers = getUserNumbers();
        System.out.println("Mean number is: " + computeTheArithmeticalMean(userNumbers));
    }

}
