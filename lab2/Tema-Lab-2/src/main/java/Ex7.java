import java.util.Arrays;
import java.util.Scanner;

public class Ex7 {

    public static void generateARandom() {
        int min=1;
        int max=10;
        int r=(int) (Math.random() * (max-min))+min;

        Scanner scan=new Scanner(System.in);
        int gues,tries=1,c[]=new int[max],OK=0,last=-1;
        Arrays.fill(c,0);

        do{
            System.out.println("Start guessing a number between "+min+"-"+max);
            gues= scan.nextInt();
            while(gues<min || gues>max)
            {
                System.out.println("Start guessing a number between "+min+"-"+max);
                gues=scan.nextInt();
            }
            if(gues!=r)
            {
                if(last!=gues)
                    tries++;
                c[gues]++;
                if(gues<r)
                    System.out.println("Too small");
                else System.out.println("Too large");
                last=gues;
            }
            else{
                System.out.println("You nailed it! "+r +" it's the number!");
                OK=1;
            }
        }while(OK==0);
        System.out.println("It took you "+tries+" tries!");
    }

    public static void main(String[] args) {
        generateARandom();
    }
}
