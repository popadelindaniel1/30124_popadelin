import java.util.Scanner;

public class Ex6 {

    /**
     * This method should generate the required vector non-recursively
     *
     * @param n the length of the generated vector
     * @return the generated vector
     */
    public static int[] generateVector(int n) {
        int mul[]=new int[n];
        mul[0]=1;
        mul[1]=2;
        for(int i=2;i<n;i++)
            mul[i]=mul[i-1]*mul[i-2];
        return mul;
    }

    /**
     * This method should generate the required vector recursively
     *
     * @param n the length of teh generated vector
     * @return the generated vector
     */
    public static int rec(int n){
        if(n<=1)
            return n+1;
        return rec(n-1) * rec(n-2);

    }
    public static int[] generateRandomVectorRecursively(int n) {
        int mul[]=new int[n];
        mul[0]=1;
        mul[1]=2;
        for(int i=2;i<n;i++)
        {
            mul[i]=rec(i);
        }
        return mul;
    }

    public static void main(String[] args) {
        // TODO: print the vectors
        int n=0;
        Scanner scan=new Scanner(System.in);
        while(n<8){
            System.out.println("Dati numarul de elemente ale vectorului: ");
            n= scan.nextInt();
        }
        int recursiv[]=generateRandomVectorRecursively(n);
        int nerecursiv[]=generateVector(n);
        System.out.println("Recursiv:");
        for(int i=0;i<n;i++)
            System.out.print(recursiv[i]+" ");
        System.out.println("\nNerecursiv:");
        for(int i=0;i<n;i++)
            System.out.print(nerecursiv[i]+" ");
    }
}